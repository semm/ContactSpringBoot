/**
 * 
 */
package com.contact.springboot.gradle.contact.service;

import org.springframework.stereotype.Service;

/**
 * @author Android
 *
 */
public interface ContactsService {
	boolean add(String name);
	
	boolean findAll(String name);
}
