/**
 * 
 */
package com.contact.springboot.gradle.contact.repository;

import java.util.List;

import com.contact.springboot.gradle.contact.entities.Person;

/**
 * @author Android
 *
 */
public interface ContactsRepository {
	
	String findById(int personId);

	String getTable();
	
	public List<Person> findAll();
	
	boolean insert(String name);
}
