/**
 * 
 */
package com.contact.springboot.gradle.contact.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.contact.springboot.gradle.contact.service.ContactsService;

/**
 * @author Android
 *
 */
@RestController
public class ContactsController {
	
	@Autowired
	public ContactsService contactService;
	
	@GetMapping("/status")
	public String status() {
		String str = contactService.add("add") ? "OK !!!" : "Not Ok ...";
		return str;
	}
	
	@GetMapping("/table")
	public String getTable() {
		String str = contactService.add("add") ? "OK !!!" : "Not Ok ...";
		return str;
	}
}

