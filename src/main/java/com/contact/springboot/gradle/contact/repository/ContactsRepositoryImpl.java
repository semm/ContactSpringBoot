/**
 * 
 */
package com.contact.springboot.gradle.contact.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.contact.springboot.gradle.contact.entities.Person;
import com.contact.springboot.gradle.contact.entities.PersonRowMapper;

/**
 * @author Android
 *
 */
@Repository
public class ContactsRepositoryImpl implements ContactsRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public String findById(int personId) {
		String sql = "select * from persons where id = ?";
		jdbcTemplate.query(sql, new Object[] {personId}, new PersonRowMapper());
		return null;
	}

	@Override
	public boolean insert(String name) {
		int updated = jdbcTemplate.update("insert into persons (name) values (?)", name);
		return updated > 0 ;
	}

	@Override
	public String getTable() {
		String sql = "select * from persons where id = ?";
		jdbcTemplate.query(sql, new PersonRowMapper());
		return null;
	}
	
	public List<Person> findAll(){
		
		String sql = "SELECT * FROM CUSTOMER";
			
		List<Person> Persons  = jdbcTemplate.query(sql,
				new BeanPropertyRowMapper(Person.class));
			
		return Persons;
	}
}